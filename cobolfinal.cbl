       IDENTIFICATION DIVISION. 
       PROGRAM-ID. cobol-final.
       AUTHOR. 62160319.
       
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADER-FILE ASSIGN TO "trader9.dat"
              ORGANIZATION IS SEQUENTIAL. 
      *    SELECT TRADER-FILE ASSIGN TO "trader.rpt"
      *       ORGANIZATION IS SEQUENTIAL. 

       DATA DIVISION.
       FILE SECTION.
       FD  TRADER-FILE
           RECORD IS VARYING IN SIZE
           DEPENDING ON TRADER-LENGTH.
       01  TRADER-REC    PIC X(40).
           88 END-OF-FILE VALUE HIGH-VALUE.

       WORKING-STORAGE SECTION. 
       01  TRADER-ID   PIC X(20).
       01  TRADER-LENGTH PIC 99.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT TRADER-FILE.
           READ TRADER-FILE 
             AT END SET END-OF-FILE TO TRUE
           END-READ.
           PERFORM UNTIL END-OF-FILE
              DISPLAY "***" TRADER-REC(1:TRADER-ID) "***"
              READ TRADER-FILE
                 AT END SET END-OF-FILE TO TRUE
              END-READ
           END-PERFORM 

           CLOSE TRADER-FILE 
           STOP RUN.      
       